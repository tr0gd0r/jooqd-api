local cjson = require "cjson"
local utils = require "utils"

local json_encode = cjson.encode
local json_decode = cjson.decode
local get_method = ngx.req.get_method
local time = os.time

local playlist = {}

local function playlists(redis_conn, args)
  local methods = {}

  local function get_playlists()
    local data = utils.get_request_data()
    local lat = data["latitude"]
    local long = data["longtitude"]
    local geo_key = "playlists_by_geo"
    local data, err = redis_conn:georadius(geo_key, long, lat, "100", "ft", "ASC")
    if not data then
      return ngx.exit(400)
    end

    local playlist_ids = {}
    for i, val in ipairs(data) do
      local playlist_id_name = utils.string_split(':', val)
      table.insert(playlist_ids, playlist_id_name[2])
    end

    local playlist_id_and_name = {}
    for i, playlist_id in ipairs(playlist_ids) do
      local playlist_key = "playlist:" .. playlist_id
      local data, err = redis_conn:hmget(playlist_key, "playlist_name", "vendor", "region_code")
      if data then
        local playlist_name = data[1]
        local vendor = data[2]
        local region_code = data[3]
        table.insert(playlist_id_and_name, {
          playlist_id = playlist_id,
          playlist_name = playlist_name,
          vendor = vendor,
          region_code = region_code
        })
      end
    end

    local res = playlist_id_and_name
    -- return type: array
    if #res == 0 then
      return cjson.empty_array
    else
      return res
    end
  end

  local function new_playlist()
    local data = utils.get_request_data()
    if not data then
      return ngx.exit(400)
    end
    local vendor = data["vendor"]
    -- for apple access_token is nil, in redis is "userdata: NULL"
    local access_token = data["vendor_access_token"]
    local region_code = data["vendor_region_code"]
    local playlist_name = data["playlist_name"]
    local latitude = data["latitude"]
    local longtitude = data["longtitude"]
    if not playlist_name then
      return ngx.exit(400)
    end
    -- todo: check if id already exists in redis, try again if it does
    local playlist_id = utils.generate_playlist_id()
    local ts = time()
    local playlist = {
      vendor = vendor,
      access_token = access_token,
      region_code = region_code,
      playlist_name = playlist_name,
      playlist_id = playlist_id,
      created_on = ts,
      latitude = latitude,
      longtitude = longtitude
    }

    -- start transaction
    local ok, err = redis_conn:multi()
    if not ok then
      return ngx.exit(400)
    end
    
    -- add playlist to geo set
    -- GEOADD key long lat member
    local geo_key = "playlists_by_geo"
    local playlist_key = "playlist:" .. playlist_id
    local member = playlist_key
    local ok, err = redis_conn:geoadd(geo_key, longtitude, latitude, member)
    if not ok then
      return ngx.exit(400)
    end

    -- add playlist
    local ok, err = redis_conn:hmset(playlist_key, playlist)
    if not ok then
      return ngx.exit(400)
    end

    -- commit
    local ok, err = redis_conn:exec()
    if not ok then
      return ngx.exit(400)
    end

    return playlist
  end

  local function get_playlist_metadata(playlist_id)
    local playlist_key = "playlist:" .. playlist_id
    local playlist_hash, err = redis_conn:hgetall(playlist_key)
    if not playlist_hash then
      return ngx.exit(400)
    end
    local playlist_metadata = {}
    local keys_to_strip = { "access_token", "longtitude", "latitude" }
    for j=1, #playlist_hash, 2 do
      playlist_metadata[playlist_hash[j]] = playlist_hash[j + 1]
    end
    for i, key in ipairs(keys_to_strip) do
      playlist_metadata[key] = nil
    end
    playlist_metadata["created_on"] = tonumber(playlist_metadata["crated_on"])
    return playlist_metadata
  end

  methods.search = get_playlists
  methods.new = new_playlist
  methods.metadata = get_playlist_metadata

  local path = args[1]
  return methods[path](args[2])
end

playlist.playlists = playlists

return playlist
