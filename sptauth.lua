local cjson = require "cjson"
local http = require "resty.http"
local aes = require "resty.aes"
local utils = require "utils"
local str = require "resty.string"

local json_decode = cjson.decode
local json_encode = cjson.encode
local get_form_data = utils.get_form_data
local encode_base64 = ngx.encode_base64
local decode_base64 = ngx.decode_base64
local sptauth = {}
local spt_client_id = "8cea2e9e922a4abdbbbe38df505f8f84"
local spt_client_secret = SPT_CLIENT_SECRET
local spt_client_callback_url = "jooqd-login://returnafterlogin"
local spt_auth_header = "Basic " .. ngx.encode_base64(spt_client_id  .. ":" .. spt_client_secret)
local spt_account_endpoint = "https://accounts.spotify.com"

local key = "cFJLyifeUJUBFWdHzVbykfDmPHtLKLGzViHW9aHGmyTLD8hGXC"
local aes_128_cbc_md5 = aes:new(key)

local function routes(redis_conn, args)
	local paths = {}

	local function swap()
		local post = get_form_data()
		-- The code returned from Spotify account service to be used in the token request.
		local code = post["code"]

		local conn = http.new()
		local res, err = conn:request_uri(spt_account_endpoint .. "/api/token", {
			method = "POST",
			body = "grant_type=authorization_code&redirect_uri=" .. spt_client_callback_url .. "&code=" .. code,
			ssl_verify = false, -- probably not a good idea to do this
			headers = {
				["Authorization"] = spt_auth_header,
				["Content-Type"] = "application/x-www-form-urlencoded"
			}
		})

		if not res then
			return ngx.exit(400)
		end

		local spt_response = json_decode(res.body)
		local refresh_token = spt_response["refresh_token"]
		local encrypted_token = aes_128_cbc_md5:encrypt(refresh_token)
		spt_response["refresh_token"] = encode_base64(encrypted_token)
    -- ngx.log(ngx.ERR, json_encode(spt_response))
		return spt_response
	end

	local function refresh()
		local post = get_form_data()
    local playlist_id = args[2]

		-- The refresh_token value previously returned from the token swap endpoint.
		local encrypted_refresh_token = decode_base64(post["refresh_token"])
		local refresh_token = aes_128_cbc_md5:decrypt(encrypted_refresh_token)

		local conn = http.new()
		local res, err = conn:request_uri(spt_account_endpoint .. "/api/token", {
			method = "POST",
			body = "grant_type=refresh_token&refresh_token=" .. refresh_token,
			ssl_verify = false, -- probably not a good idea to do this
			headers = {
				["Authorization"] = spt_auth_header,
				["Content-Type"] = "application/x-www-form-urlencoded"
			}
		})

		if not res then
			return ngx.exit(400)
		end
    
    local spt_res_body = json_decode(res.body)
    local key = "playlist:" .. playlist_id
    local ok, err = redis_conn:hset(key, "access_token", spt_res_body["access_token"])
    if not ok then
      return ngx.exit(400)
    end

		return json_decode(res.body)
	end

	paths.swap = swap
	paths.refresh = refresh

	return paths[args[1]]()
end

sptauth.routes = routes

return sptauth
