local cjson = require "cjson"
local http = require "resty.http"

local json_encode = cjson.encode
local json_decode = cjson.decode
local get_method = ngx.req.get_method
local time = os.time

local search = {}

local function sptquery(redis_conn, args)
  local methods = {}
  
  local function search_tracks()
    local playlist_id = args[1]
    local query_term = args[2]
    local playlist_key = "playlist:" .. playlist_id

    local data, err = redis_conn:hget(playlist_key, "access_token")
    if not data then
      return ngx.exit(400)
    end
    local oauth_token = data

    local conn = http.new()
    local res, err = conn:request_uri("https://api.spotify.com/v1/search", {
      method = "GET",
      query = "limit=10&market=US&type=track&q=" .. query_term,
      ssl_verify = false, -- probably not a good idea to do this
      headers = {
        ["Accept"] = "application/json",
        ["Authorization"] = "Bearer " .. oauth_token
      }
    })

    if not res then
      ngx.say(err)
      return ngx.exit(400)
    end

    local decoded_body = json_decode(res.body)
    return decoded_body["tracks"]["items"]
  end

  methods.GET = search_tracks

  local method = get_method()
  return methods[method]()
end

search.sptquery = sptquery

return search
