local error = {}

local function error_logger(redis_conn, args)
  local error_code = args[1]
  ngx.log(ngx.ERR, "logged error code: " .. error_code)
  return ngx.exit(204)
end

error.log = error_logger

return error
