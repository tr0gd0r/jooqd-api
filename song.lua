local cjson = require "cjson"
local utils = require "utils"

local song = {}

local get_method = ngx.req.get_method
local time = os.time

local function songs(redis_conn, args)
  local methods = {}
  
  local function get_songs(playlist_id)
    local songs_list_key = "songs:" .. playlist_id
    local data, err = redis_conn:smembers(songs_list_key)
    if not data then
      return ngx.exit(400)
    end

    local res = {}
    for i, song_id in ipairs(data) do
      -- hgetall returns array that looks like [key1, val1, key2, val2]
      -- so it needs to be converted into {key1=val1, key2=val2}
      local song_object_key = "playlist:" .. playlist_id .. ":song:" .. song_id
      local songs_hash, err = redis_conn:hgetall(song_object_key)
      if songs_hash then
        local song = {}
        for j=1, #songs_hash, 2 do
          song[songs_hash[j]] = songs_hash[j + 1]
        end
        -- votes and created_on are int
        song["votes"] = tonumber(song["votes"])
        song["created_on"] = tonumber(song["crated_on"])
        table.insert(res, song)
      end
    end
    
    -- return type: array
    if #res == 0 then
      return cjson.empty_array
    else
      return res
    end
  end

  local function new_song()
    local data = utils.get_request_data()
    if not data then
      return ngx.exit(400)
    end

    local vendor = data["vendor"]
    local playlist_id = data["playlist_id"]
    local song_id = data["song_id"]
    local song_name = data["song_name"]
    local artist = data["artist"]
    local album = data["album"]
    local album_art_url = data["album_art_url"]
    local votes = 0
    local created_on = time()

    local songs_list_key = "songs:" .. playlist_id
    local song_object_key = "playlist:" .. playlist_id .. ":song:" .. song_id

    -- if song is already in the playlist, return 409
    local ok, err = redis_conn:exists(song_object_key)
    if not ok then
      return ngx.exit(400)
    end
    if ok == 1 then
      return ngx.exit(409)
    end
    
    -- start transaction
    local ok, err = redis_conn:multi()
    if not ok then
      return ngx.exit(400)
    end
    -- add song to list of all songs for that playlist
    local ok, err = redis_conn:sadd(songs_list_key, song_id)
    if not ok then
      return ngx.exit(400)
    end
    -- add playlist
    local song = {
      vendor = vendor,
      playlist_id = playlist_id,
      song_id = song_id,
      song_name = song_name,
      artist = artist,
      album = album,
      album_art_url = album_art_url,
      votes = votes,
      created_on = created_on
    }
    local ok, err = redis_conn:hmset(song_object_key, song)
    if not ok then
      return ngx.exit(400)
    end
    -- commit
    local ok, err = redis_conn:exec()
    if not ok then
      return ngx.exit(400)
    end

    return song
  end

  local function change_vote(playlist_id, song_id, direction)
    local song_object_key = "playlist:" .. playlist_id .. ":song:" .. song_id
    if direction == "up" then
      local ok, err = redis_conn:hincrby(song_object_key, "votes", 1)
      if not ok then
        return ngx.exit(400)
      end
    elseif direction == "down" then
      local ok, err = redis_conn:hincrby(song_object_key, "votes", -1)
      if not ok then
        return ngx.exit(400)
      end
    end
    -- PATCH + 202 caused browser to emit "No 'Access-Control-Allow-Origin' header" error
    -- ngx.status = ngx.HTTP_ACCEPTED
    return {
      playlist_id = playlist_id,
      song_id = song_id,
      direction = direction
    }
  end

  local function top_song(playlist_id)
    local songs_list_key = "songs:" .. playlist_id
    local data, err = redis_conn:smembers(songs_list_key)
    if not data then
      return ngx.exit(400)
    end

    local all_songs = {}
    for i, song_id in ipairs(data) do
      -- hgetall returns array that looks like [key1, val1, key2, val2]
      -- so it needs to be converted into {key1=val1, key2=val2}
      local song_object_key = "playlist:" .. playlist_id .. ":song:" .. song_id
      local songs_hash, err = redis_conn:hgetall(song_object_key)
      if songs_hash then
        local song = {}
        for j=1, #songs_hash, 2 do
          song[songs_hash[j]] = songs_hash[j + 1]
        end
        -- votes and crated_on are int
        song["votes"] = tonumber(song["votes"])
        song["crated_on"] = tonumber(song["crated_on"])
        table.insert(all_songs, song)
      end
    end

    if #all_songs == 0 then
      return ngx.exit(202)
    end

    -- return song with highest number of votes
    local song_most_votes = nil
    for i, song in ipairs(all_songs) do
      if song_most_votes == nil then
        song_most_votes = song
      else
        if song["votes"] > song_most_votes["votes"] then
          song_most_votes = song
        end
      end
    end

    if song_most_votes == nil then
      return ngx.exit(204)
    else
      local song_object_key = "playlist:" .. playlist_id .. ":song:" .. song_most_votes["song_id"]
      local songs_list_key = "songs:" .. song_most_votes["playlist_id"]
      --
      -- start transaction
      local ok, err = redis_conn:multi()
      if not ok then
        return ngx.exit(400)
      end

      -- delete song
      local ok, err = redis_conn:del(song_object_key)
      if not ok then
        return ngx.exit(400)
      end

      -- delete song id from all songs set
      local ok, err = redis_conn:srem(songs_list_key, song_most_votes["song_id"])
      if not ok then
        return ngx.exit(400)
      end

      -- commit
      local ok, err = redis_conn:exec()
      if not ok then
        return ngx.exit(400)
      end

      return song_most_votes
    end
  end

  local method = get_method()

  if method == "GET" then
    if #args == 1 then
      local playlist_id = args[1]
      return get_songs(playlist_id)
    else
      local playlist_id = args[1]
      return top_song(playlist_id)
    end
  elseif method == "POST" then
    return new_song()
  elseif method == "PATCH" then
    local playlist_id = args[1]
    local song_id = args[2]
    local direction = args[3]
    return change_vote(playlist_id, song_id, direction)
  else
    return {}
  end
end

song.songs = songs

return song
