local cjson = require "cjson"
local redis = require "resty.redis"

local utils = require "utils"
local playlist_routes = require "playlist"
local song_routes = require "song"
local search_routes = require "search"
local sptauth_routes = require "sptauth"
local error_route = require "errors"

local json_encode = cjson.encode
local json_decode = cjson.decode
local get_method = ngx.req.get_method
local time = os.time

local routes = {}

routes.playlists = playlist_routes.playlists
routes.songs = song_routes.songs
routes.sptquery = search_routes.sptquery
routes.sptauth = sptauth_routes.routes
routes.err = error_route.log

--
-- MAIN
--
local redis_conn, err = redis:new()
if not redis_conn then
  return ngx.exit(400)
end

redis_conn:set_timeout(1000)

local ok, err = redis_conn:connect("127.0.0.1", 6379)
if not ok then
  return ngx.exit(400)
end

local path_params = utils.string_split('/', ngx.var.request_uri)
local route = table.remove(path_params, 1)
if route == "/" then
  return ngx.exit(204)
end
local args = path_params

local response = routes[route](redis_conn, args)
ngx.say(json_encode(response))


