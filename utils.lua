local cjson = require "cjson"

local utils = {}

local json_decode = cjson.decode
local read_body = ngx.req.read_body
local get_body_data = ngx.req.get_body_data
local get_body_file = ngx.req.get_body_file
local get_post_args = ngx.req.get_post_args
local random = math.random
local randomseed = math.randomseed
local time = os.time

-- taken from http://lua-users.org/wiki/SplitJoin
-- d = seperator
-- p = string
local function string_split(d,p)
  local t, ll
  t={}
  ll=0
  if(#p == 1) then
    return {p}
  end
  while true do
    l = string.find(p, d, ll, true) -- find the next d in the string
    if l ~= nil then -- if "not not" found then..
      match = string.sub(p,ll,l-1)
      if match ~= "" then
        table.insert(t, match) -- Save it in our array.
      end
      ll = l + 1 -- save just after where we found it for searching next time.
    else
      match = string.sub(p,ll)
      if match ~= "" then
        table.insert(t, match) -- Save what's left in our array.
      end
      break -- Break at end, as it should be, according to the lua manual.
    end
  end
  return t
end

local function get_request_data()
    read_body()
    local body_data = get_body_data()
    if body_data then
      return json_decode(body_data)
    end
    ngx.log(ngx.ERR, body_data)
    return nil

    -- from docs:
    -- "body may get buffered in a temp file"
    -- ignoring for now...
end

local function generate_playlist_id()
  local length = 4
  local chars = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z', '1', '2', '3', '4', '5', '6', '7', '8', '9'}
  local playlist_id = ""
  -- from http://lua-users.org/wiki/MathLibraryTutorial
  randomseed(time())
  for i=1,10 do
    random(#chars)
  end
  --
  for i = 1, length do
    local num = random(#chars)
    local rand_char = chars[num]
    playlist_id = playlist_id .. rand_char
  end
  return playlist_id
end

local function get_form_data()
  read_body()
  local args, err = get_post_args()
  if not args then
    ngx.exit(400)
    return
  end
  return args
end

utils.string_split = string_split
utils.get_request_data = get_request_data
utils.generate_playlist_id = generate_playlist_id
utils.get_form_data = get_form_data

return utils
